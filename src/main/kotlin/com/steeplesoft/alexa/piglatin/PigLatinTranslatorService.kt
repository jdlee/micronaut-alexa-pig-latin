package com.steeplesoft.alexa.piglatin

import java.util.stream.Collectors
import javax.inject.Singleton

@Singleton
class PigLatinTranslatorService {
    companion object {
        val VOWELS = listOf("a", "e", "i", "o", "u")
    }

    fun translate(phrase: String): String {
        val parts = phrase.split(" ")
        val foo = parts
                .map {
                    handleToken(it)
                }

        return foo.joinToString(" ")
    }

    private fun handleToken (token : String) : String {
        if (VOWELS.contains(token.first().toString())) {
            return token + "way"
        } else {
            return token.substring(1) + token.subSequence(0,1) + "ay"
        }
    }
}