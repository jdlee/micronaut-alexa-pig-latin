package com.steeplesoft.alexa.piglatin

import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.dispatcher.request.handler.RequestHandler;
import com.amazon.ask.model.LaunchRequest;
import com.amazon.ask.model.Response;
import com.amazon.ask.request.Predicates;

import javax.inject.Singleton;
import java.util.Optional;

@Singleton
class LaunchRequestHandler : RequestHandler {
    override fun canHandle(input: HandlerInput): Boolean {
        return input.matches(Predicates.requestType(LaunchRequest::class.java))
    }

    override fun handle(input: HandlerInput): Optional<Response> {
        val speechText = "Welcome to Jason's Pig Latin Translator. You can give me a phrase to translate.";
        return input.responseBuilder
                .withSpeech(speechText)
                .withSimpleCard(AlexaApplication.INTENT_NAME, speechText)
                .withReprompt(speechText)
                .build()
    }
}