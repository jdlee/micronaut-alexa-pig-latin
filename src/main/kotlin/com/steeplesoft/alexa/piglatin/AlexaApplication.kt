package com.steeplesoft.alexa.piglatin

import com.amazon.ask.dispatcher.request.handler.HandlerInput
import com.amazon.ask.model.IntentRequest
import com.amazon.ask.model.Response
import io.micronaut.function.aws.alexa.AlexaIntents
import io.micronaut.function.aws.alexa.annotation.IntentHandler
import java.util.*
import javax.inject.Singleton


@Singleton
class AlexaApplication(private val translatorService: PigLatinTranslatorService) {
    companion object {
        const val INTENT_NAME = "TranslateIntent"
        const val CARD_TITLE = "Pig Latin"
        const val SLOT_QUERY = "Query"
    }

    @IntentHandler(INTENT_NAME)
    fun translate (input : HandlerInput) : Optional<Response> {
        val intentRequest = input.requestEnvelope.request as IntentRequest
        val slots = intentRequest.intent.slots

        val phrase = slots[SLOT_QUERY]
        val translated =
                if (phrase != null) translatorService.translate(phrase.value) else "I'm sorry. I don't understand."

        return input.responseBuilder
                .withSpeech(translated)
                .withSimpleCard(CARD_TITLE, translated)
                .build()
    }

    @IntentHandler(AlexaIntents.HELP)
    fun help(input : HandlerInput ) : Optional<Response> {
        val speechText = "You can give me a phrase to translate to Pig Latin!"
        return input.responseBuilder
                .withSpeech(speechText)
                .withSimpleCard(CARD_TITLE, speechText)
                .withReprompt(speechText)
                .build()
    }

    @IntentHandler(AlexaIntents.FALLBACK)
    fun fallback(input : HandlerInput) : Optional<Response>  {
        val speechText = "Sorry, I don't know that. You can say try saying help!"
        return input.responseBuilder
                .withSpeech(speechText)
                .withSimpleCard(CARD_TITLE, speechText)
                .withReprompt(speechText)
                .build()
    }

    @IntentHandler(AlexaIntents.CANCEL, AlexaIntents.STOP)
    fun cancel(input : HandlerInput) : Optional<Response> {
        return input.responseBuilder
                .withSpeech("Goodbye")
                .withSimpleCard(CARD_TITLE, "Goodbye")
                .build()
    }
}