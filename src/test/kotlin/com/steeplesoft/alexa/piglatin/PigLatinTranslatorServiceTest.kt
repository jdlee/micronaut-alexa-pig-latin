package com.steeplesoft.alexa.piglatin

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class PigLatinTranslatorServiceTest {
    val service = PigLatinTranslatorService()

    @Test
    fun testConsonant() {
        assertEquals("ellohay", service.translate("hello"))
    }

    @Test
    fun testVowel() {
        assertEquals("awesomeway", service.translate("awesome"))
    }

    @Test
    fun testPhrase() {
        assertEquals("igpay atinlay isway unfay", service.translate("pig latin is fun"))
    }
}