package com.steeplesoft.alexa.piglatin

import com.amazon.ask.dispatcher.request.handler.HandlerInput
import com.amazon.ask.model.Intent
import com.amazon.ask.model.IntentRequest
import com.amazon.ask.model.RequestEnvelope
import com.amazon.ask.model.Slot
import io.micronaut.test.annotation.MicronautTest
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import javax.inject.Inject
import javax.inject.Singleton

@MicronautTest
class AlexaApplicationTest {

    @Inject
    lateinit var handler : AlexaApplication

    @Test
    fun testHelloWorldIntent()  {
        val builder = HandlerInput.builder()
        val envelopeBuilder = RequestEnvelope.builder()
        val intentRequest = IntentRequest.builder().withIntent(
                Intent.builder().putSlotsItem(AlexaApplication.SLOT_QUERY,
                        Slot.builder()
                                .withName(AlexaApplication.SLOT_QUERY)
                                .withValue("Hello world")
                                .build())
                        .build()
        )

        envelopeBuilder.withRequest(intentRequest.build())
        builder.withRequestEnvelope(envelopeBuilder.build())

        val response = handler.translate(builder.build())
        assertTrue(response.isPresent)

        val outputSpeech = response.get().outputSpeech
        assertTrue(
                outputSpeech.toString().contains(
                        "elloHay orldway"
                )
        )
    }
}